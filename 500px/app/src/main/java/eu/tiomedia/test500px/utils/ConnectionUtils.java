package eu.tiomedia.test500px.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import eu.tiomedia.test500px.R;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
public class ConnectionUtils {

    public static boolean isInternetAvailable(final Context context, boolean showErrorToast) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        boolean enabled = (networkInfo != null && networkInfo.isConnected());

        if (!enabled && showErrorToast) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, R.string.error_message_lost_connection,
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
        return enabled;
    }
}
