package eu.tiomedia.test500px.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import eu.tiomedia.test500px.fragments.PhotoFragment;
import eu.tiomedia.test500px.rest.models.Photo;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
public class PhotoPagerAdapter extends FragmentStatePagerAdapter {

    private List<Photo> mData = new ArrayList<>();

    public PhotoPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return PhotoFragment.newInstance(mData.get(position).getImageUrl());
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    public void setData(List<Photo> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }
}
