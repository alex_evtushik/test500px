package eu.tiomedia.test500px.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.tiomedia.test500px.R;
import eu.tiomedia.test500px.listeners.OnClickRecycleItemListener;
import eu.tiomedia.test500px.rest.models.Photo;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
public class PhotoRecyclerAdapter extends RecyclerView.Adapter<PhotoRecyclerAdapter.PhotoViewHolder> {
    private List<Photo> mPhotos = new ArrayList<>();
    private OnClickRecycleItemListener mListener;
    private Context mContext;

    public PhotoRecyclerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false);
        return new PhotoViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mPhotos.size();
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        holder.fill(mPhotos.get(position));
    }

    public void setData(List<Photo> data) {
        mPhotos.clear();
        mPhotos.addAll(data);
        notifyDataSetChanged();
    }

    public List<Photo> getData() {
        return mPhotos;
    }

    public void setOnClickRecycleItemListener(OnClickRecycleItemListener listener) {
        mListener = listener;
    }

    class PhotoViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.item_image)
        ImageView mPhoto;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void fill(Photo photo) {
            Glide.with(mContext)
                    .load(photo.getImageUrl())
                    .fallback(R.mipmap.ic_launcher)
                    .placeholder(new ColorDrawable(Color.GRAY))
                    .transform(new CenterCrop(mContext))
                    .thumbnail(0.1f)
                    .crossFade()
                    .into(mPhoto);
        }

        @OnClick(R.id.item_image)
        void onOpenPhoto() {
            if (mListener != null) {
                mListener.onClick(mPhoto, getLayoutPosition());
            }
        }
    }
}
