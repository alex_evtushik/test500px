package eu.tiomedia.test500px.rest.models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
@Parcel
public class Photo {

    @SerializedName("id")
    private int mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("times_viewed")
    private int mTimesViewed;

    @SerializedName("rating")
    private double mRating;

    @SerializedName("created_at")
    private String mCreatedAt;

    @SerializedName("category")
    private int mCategory;

    @SerializedName("privacy")
    private boolean mPrivacy;

    @SerializedName("width")
    private int mWidth;

    @SerializedName("height")
    private int mHeight;

    @SerializedName("votes_count")
    private int mVotesCount;

    @SerializedName("favorites_count")
    private int mFavoritesCount;

    @SerializedName("comments_count")
    private int mCommentsCount;

    @SerializedName("nsfw")
    private boolean mNsfw;

    @SerializedName("image_url")
    private String mImageUrl;

    @SerializedName("user")
    private User mUser;

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getTimesViewed() {
        return mTimesViewed;
    }

    public double getRating() {
        return mRating;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public int getCategory() {
        return mCategory;
    }

    public boolean isPrivacy() {
        return mPrivacy;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getVotesCount() {
        return mVotesCount;
    }

    public int getFavoritesCount() {
        return mFavoritesCount;
    }

    public int getCommentsCount() {
        return mCommentsCount;
    }

    public boolean isNsfw() {
        return mNsfw;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public User getUser() {
        return mUser;
    }
}
