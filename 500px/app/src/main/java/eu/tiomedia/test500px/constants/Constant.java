package eu.tiomedia.test500px.constants;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
public class Constant {
    public static final String URL_ROOT = "https://api.500px.com/v1/";
    public static final String CONSUMER_API_KEY = "FF84D56OP4toVfP3UfmAZkqOFWUgvAt3yVaY21Of";

    public static final String KEY_PHOTOS = "photos";
    public static final String KEY_FEATURE = "feature";
    public static final String KEY_CONSUMER = "consumer_key";
    public static final String KEY_POPULAR = "popular";
    public static final String KEY_FRESH_TODAY = "fresh_today";
    public static final String KEY_POSITION = "position";
    public static final String KEY_IMAGE_URL = "image_url";

    public static final String KEY_RPP = "rpp";
    public static final int VALUE_RPP = 100;
    public static final int HTTP_OK = 200;
}
