package eu.tiomedia.test500px.rest;

import eu.tiomedia.test500px.constants.Constant;
import eu.tiomedia.test500px.rest.models.ResponsePhotos;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Query;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
public interface ApiService {

    @Headers({"Content-Type: application/json; charset=utf-8"})
    @GET(Constant.KEY_PHOTOS)
    Call<ResponsePhotos> loadPhotos(@Query(Constant.KEY_FEATURE) String feature,
                                    @Query(Constant.KEY_CONSUMER) String consumerKey,
                                    @Query(Constant.KEY_RPP) int rpp);
}
