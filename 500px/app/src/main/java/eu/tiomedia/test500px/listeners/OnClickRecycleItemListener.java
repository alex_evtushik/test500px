package eu.tiomedia.test500px.listeners;

import android.view.View;

/**
 * Created by Alexandr Evtushik on 23.10.15.
 */
public interface OnClickRecycleItemListener {

    void onClick(View view, int position);
}
