package eu.tiomedia.test500px.rest.models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
@Parcel
public class User {

    @SerializedName("id")
    private int mId;

    @SerializedName("username")
    private String mUsername;

    @SerializedName("firstname")
    private String mFirstname;

    @SerializedName("lastname")
    private String mLastname;

    @SerializedName("country")
    private String mCountry;

    @SerializedName("fullname")
    private String mFullname;

    @SerializedName("userpic_url")
    private String mUserpicUrl;

    @SerializedName("upgrade_status")
    private int mUpgradeStatus;

    public int getId() {
        return mId;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getFirstname() {
        return mFirstname;
    }

    public String getLastname() {
        return mLastname;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getFullname() {
        return mFullname;
    }

    public String getUserpicUrl() {
        return mUserpicUrl;
    }

    public int getUpgradeStatus() {
        return mUpgradeStatus;
    }
}
