package eu.tiomedia.test500px.rest;

import com.squareup.okhttp.OkHttpClient;

import eu.tiomedia.test500px.constants.Constant;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
public class RestClient {
    private ApiService mApiService;

    public RestClient() {
        mApiService = new Retrofit.Builder()
                .baseUrl(Constant.URL_ROOT)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiService.class);
    }

    public ApiService getApiService() {
        return mApiService;
    }
}
