package eu.tiomedia.test500px.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.parceler.Parcels;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import eu.tiomedia.test500px.R;
import eu.tiomedia.test500px.adapters.PhotoPagerAdapter;
import eu.tiomedia.test500px.constants.Constant;
import eu.tiomedia.test500px.rest.models.Photo;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
public class PhotoPagerActivity extends AppCompatActivity {
    @Bind(R.id.view_pager_photo)
    ViewPager mViewPager;

    public static void start(Activity activity, View transitionView, List<Photo> data, int position) {
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                activity, transitionView, Constant.KEY_PHOTOS);

        Intent starter = new Intent(activity, PhotoPagerActivity.class);
        starter.putExtra(Constant.KEY_PHOTOS, Parcels.wrap(data));
        starter.putExtra(Constant.KEY_POSITION, position);

        ActivityCompat.startActivity(activity, starter, options.toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_pager);
        ButterKnife.bind(this);
        PhotoPagerAdapter mAdapter = new PhotoPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        ViewCompat.setTransitionName(mViewPager, Constant.KEY_PHOTOS);

        if (getIntent().hasExtra(Constant.KEY_PHOTOS)) {
            mAdapter.setData(Parcels.<List<Photo>>unwrap(getIntent().getParcelableExtra(Constant.KEY_PHOTOS)));
            mViewPager.setCurrentItem(getIntent().getIntExtra(Constant.KEY_POSITION, 0));
        }
    }
}
