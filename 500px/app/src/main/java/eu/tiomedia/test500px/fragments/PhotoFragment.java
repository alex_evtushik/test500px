package eu.tiomedia.test500px.fragments;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.FitCenter;

import butterknife.Bind;
import butterknife.ButterKnife;
import eu.tiomedia.test500px.R;
import eu.tiomedia.test500px.constants.Constant;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
public class PhotoFragment extends Fragment {
    public static final String TAG = PhotoFragment.class.getSimpleName();
    @Bind(R.id.image)
    ImageView mImage;

    public static PhotoFragment newInstance(String url) {
        Bundle args = new Bundle();
        args.putString(Constant.KEY_IMAGE_URL, url);
        PhotoFragment fragment = new PhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String url = getArguments().getString(Constant.KEY_IMAGE_URL, "");
        Glide.with(getActivity())
                .load(url)
                .fallback(R.mipmap.ic_launcher)
                .placeholder(new ColorDrawable(Color.GRAY))
                .transform(new FitCenter(getActivity()))
                .thumbnail(0.1f)
                .crossFade()
                .into(mImage);
    }
}
