package eu.tiomedia.test500px.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Bind;
import butterknife.ButterKnife;
import eu.tiomedia.test500px.R;
import eu.tiomedia.test500px.activities.PhotoPagerActivity;
import eu.tiomedia.test500px.adapters.PhotoRecyclerAdapter;
import eu.tiomedia.test500px.constants.Constant;
import eu.tiomedia.test500px.listeners.OnClickRecycleItemListener;
import eu.tiomedia.test500px.rest.ApiService;
import eu.tiomedia.test500px.rest.RestClient;
import eu.tiomedia.test500px.rest.models.ResponsePhotos;
import eu.tiomedia.test500px.utils.ConnectionUtils;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
public class GridPhotoFragment extends Fragment implements OnClickRecycleItemListener {
    public static final String TAG = GridPhotoFragment.class.getSimpleName();
    private static final int SPAN_COUNT = 3;
    private PhotoRecyclerAdapter mAdapter;
    private ProgressDialog mDialog;

    @Bind(R.id.recycler_view)
    RecyclerView mRecyclerView;

    public static GridPhotoFragment newInstance(String feature) {
        Bundle args = new Bundle();
        args.putString(Constant.KEY_FEATURE, feature);
        GridPhotoFragment fragment = new GridPhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_app, container, false);
        ButterKnife.bind(this, rootView);
        mAdapter = new PhotoRecyclerAdapter(getActivity());
        mAdapter.setOnClickRecycleItemListener(this);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), SPAN_COUNT));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setAdapter(mAdapter);
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initProgressDialog();
        RestClient restClient = new RestClient();
        ApiService apiService = restClient.getApiService();
        String feature = getArguments().getString(Constant.KEY_FEATURE);
        showProgressDialog();
        if (!ConnectionUtils.isInternetAvailable(getActivity(), true)) return;
        apiService.loadPhotos(feature, Constant.CONSUMER_API_KEY, Constant.VALUE_RPP).enqueue(new Callback<ResponsePhotos>() {
            @Override
            public void onResponse(Response<ResponsePhotos> response, Retrofit retrofit) {
                cancelProgressDialog();
                if (response.code() == Constant.HTTP_OK) {
                    ResponsePhotos responsePhotos = response.body();
                    mAdapter.setData(responsePhotos.getPhotos());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                cancelProgressDialog();
            }
        });
    }

    private void initProgressDialog() {
        mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage(getResources().getString(R.string.wait_message_progress));
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setCancelable(false);
    }

    private void showProgressDialog() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    private void cancelProgressDialog() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    @Override
    public void onClick(View view, int position) {
        PhotoPagerActivity.start(getActivity(), view, mAdapter.getData(), position);
    }
}
