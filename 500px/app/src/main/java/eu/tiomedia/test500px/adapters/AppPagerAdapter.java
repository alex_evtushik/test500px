package eu.tiomedia.test500px.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import eu.tiomedia.test500px.constants.Constant;
import eu.tiomedia.test500px.fragments.GridPhotoFragment;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
public class AppPagerAdapter extends FragmentStatePagerAdapter {
    private static final int PAGER_COUNT = 2;
    private static final int POPULAR_POSITION = 0;
    private static final int FRESH_POSITION = 1;
    private static final String POPULAR_TITLE = "Popular";
    private static final String FRESH_TITLE = "Fresh";

    public AppPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            default:
            case POPULAR_POSITION:
                return GridPhotoFragment.newInstance(Constant.KEY_POPULAR);
            case FRESH_POSITION:
                return GridPhotoFragment.newInstance(Constant.KEY_FRESH_TODAY);
        }
    }

    @Override
    public int getCount() {
        return PAGER_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            default:
            case POPULAR_POSITION:
                return POPULAR_TITLE;
            case FRESH_POSITION:
                return FRESH_TITLE;
        }
    }
}
