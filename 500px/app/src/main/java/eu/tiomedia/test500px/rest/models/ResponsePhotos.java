package eu.tiomedia.test500px.rest.models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by Alexandr Evtushik on 04.12.15.
 */
@Parcel
public class ResponsePhotos {

    @SerializedName("feature")
    private String mFeature;

    @SerializedName("current_page")
    private int mCurrentPage;

    @SerializedName("total_pages")
    private int mTotalPages;

    @SerializedName("total_items")
    private int mTotalItems;

    @SerializedName("photos")
    private ArrayList<Photo> mPhotos;

    public String getFeature() {
        return mFeature;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public int getTotalPages() {
        return mTotalPages;
    }

    public int getTotalItems() {
        return mTotalItems;
    }

    public ArrayList<Photo> getPhotos() {
        return mPhotos;
    }
}
